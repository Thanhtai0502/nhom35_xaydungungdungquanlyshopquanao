﻿
namespace QuanLyShopQuanAo
{
	partial class frmAdmin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tcAdmin = new System.Windows.Forms.TabControl();
			this.tpQuanAo = new System.Windows.Forms.TabPage();
			this.panel4 = new System.Windows.Forms.Panel();
			this.txtGhiChuQA = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.btnBrowseHinhQA = new System.Windows.Forms.Button();
			this.panel25 = new System.Windows.Forms.Panel();
			this.cbLoaiQA = new System.Windows.Forms.ComboBox();
			this.label14 = new System.Windows.Forms.Label();
			this.pbHinhQA = new System.Windows.Forms.PictureBox();
			this.panel9 = new System.Windows.Forms.Panel();
			this.nmGiaBanQA = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.panel8 = new System.Windows.Forms.Panel();
			this.lblDaban = new System.Windows.Forms.Label();
			this.lblDB = new System.Windows.Forms.Label();
			this.nmSoLuongQA = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.panel7 = new System.Windows.Forms.Panel();
			this.txtSizeQA = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.panel6 = new System.Windows.Forms.Panel();
			this.txtTenQA = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.panel5 = new System.Windows.Forms.Panel();
			this.txtIDQA = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.dtgvQuanAo = new System.Windows.Forms.DataGridView();
			this.panel2 = new System.Windows.Forms.Panel();
			this.txtTimDenGiaQA = new System.Windows.Forms.TextBox();
			this.txtTimTuGiaQA = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.txtTimQA = new System.Windows.Forms.TextBox();
			this.btnTimQA = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnXoaQA = new System.Windows.Forms.Button();
			this.btnXemQA = new System.Windows.Forms.Button();
			this.btnSuaQA = new System.Windows.Forms.Button();
			this.btnThemQA = new System.Windows.Forms.Button();
			this.tpLoaiQuanAo = new System.Windows.Forms.TabPage();
			this.panel12 = new System.Windows.Forms.Panel();
			this.label19 = new System.Windows.Forms.Label();
			this.txtLoaiQA_TimKiemTen = new System.Windows.Forms.TextBox();
			this.btnLoaiQA_TimKiem = new System.Windows.Forms.Button();
			this.panel10 = new System.Windows.Forms.Panel();
			this.gbLoaiQA = new System.Windows.Forms.GroupBox();
			this.dgvSoluongLQA = new System.Windows.Forms.DataGridView();
			this.panel17 = new System.Windows.Forms.Panel();
			this.txtLoaiQA_SoLuongSP = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.panel14 = new System.Windows.Forms.Panel();
			this.txtLoaiQA_Ten = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.panel15 = new System.Windows.Forms.Panel();
			this.txtLoaiQA_ID = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.panel16 = new System.Windows.Forms.Panel();
			this.dtgvLoaiQuanAo = new System.Windows.Forms.DataGridView();
			this.panel18 = new System.Windows.Forms.Panel();
			this.btnXoaLoaiQA = new System.Windows.Forms.Button();
			this.btnXemLoaiQA = new System.Windows.Forms.Button();
			this.btnSuaLoaiQA = new System.Windows.Forms.Button();
			this.btnThemLoaiQA = new System.Windows.Forms.Button();
			this.tpKhachHang = new System.Windows.Forms.TabPage();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnKhachHang_TaiLaiDS = new System.Windows.Forms.Button();
			this.btnKhachHang_XoaBoLoc = new System.Windows.Forms.Button();
			this.txtKhachHang_TimKiem_TenSDT = new System.Windows.Forms.TextBox();
			this.panel20 = new System.Windows.Forms.Panel();
			this.panel28 = new System.Windows.Forms.Panel();
			this.txtKhachHang_DiaChi = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.panel22 = new System.Windows.Forms.Panel();
			this.txtKhachHang_SoLuongHoaDon = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.panel23 = new System.Windows.Forms.Panel();
			this.txtKhachHang_SDT = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.panel24 = new System.Windows.Forms.Panel();
			this.txtKhachHang_Ten = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.panel26 = new System.Windows.Forms.Panel();
			this.dtgvKH = new System.Windows.Forms.DataGridView();
			this.tpTaiKhoan = new System.Windows.Forms.TabPage();
			this.panel21 = new System.Windows.Forms.Panel();
			this.panel27 = new System.Windows.Forms.Panel();
			this.cbTK_Loai = new System.Windows.Forms.ComboBox();
			this.label23 = new System.Windows.Forms.Label();
			this.panel33 = new System.Windows.Forms.Panel();
			this.btnTK_Xoa = new System.Windows.Forms.Button();
			this.btnTK_Them = new System.Windows.Forms.Button();
			this.panel30 = new System.Windows.Forms.Panel();
			this.txtTK_MatKhau = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.panel31 = new System.Windows.Forms.Panel();
			this.txtTK_TenDangNhap = new System.Windows.Forms.TextBox();
			this.label26 = new System.Windows.Forms.Label();
			this.panel32 = new System.Windows.Forms.Panel();
			this.dtgvTaiKhoan = new System.Windows.Forms.DataGridView();
			this.btn_QA_AddLoai = new System.Windows.Forms.Button();
			this.tcAdmin.SuspendLayout();
			this.tpQuanAo.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel25.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pbHinhQA)).BeginInit();
			this.panel9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nmGiaBanQA)).BeginInit();
			this.panel8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nmSoLuongQA)).BeginInit();
			this.panel7.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtgvQuanAo)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.tpLoaiQuanAo.SuspendLayout();
			this.panel12.SuspendLayout();
			this.panel10.SuspendLayout();
			this.gbLoaiQA.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvSoluongLQA)).BeginInit();
			this.panel17.SuspendLayout();
			this.panel14.SuspendLayout();
			this.panel15.SuspendLayout();
			this.panel16.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtgvLoaiQuanAo)).BeginInit();
			this.panel18.SuspendLayout();
			this.tpKhachHang.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel20.SuspendLayout();
			this.panel28.SuspendLayout();
			this.panel22.SuspendLayout();
			this.panel23.SuspendLayout();
			this.panel24.SuspendLayout();
			this.panel26.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtgvKH)).BeginInit();
			this.tpTaiKhoan.SuspendLayout();
			this.panel21.SuspendLayout();
			this.panel27.SuspendLayout();
			this.panel33.SuspendLayout();
			this.panel30.SuspendLayout();
			this.panel31.SuspendLayout();
			this.panel32.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtgvTaiKhoan)).BeginInit();
			this.SuspendLayout();
			// 
			// tcAdmin
			// 
			this.tcAdmin.Controls.Add(this.tpQuanAo);
			this.tcAdmin.Controls.Add(this.tpLoaiQuanAo);
			this.tcAdmin.Controls.Add(this.tpKhachHang);
			this.tcAdmin.Controls.Add(this.tpTaiKhoan);
			this.tcAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tcAdmin.Location = new System.Drawing.Point(0, 0);
			this.tcAdmin.Name = "tcAdmin";
			this.tcAdmin.SelectedIndex = 0;
			this.tcAdmin.Size = new System.Drawing.Size(1064, 681);
			this.tcAdmin.TabIndex = 0;
			// 
			// tpQuanAo
			// 
			this.tpQuanAo.Controls.Add(this.panel4);
			this.tpQuanAo.Controls.Add(this.panel3);
			this.tpQuanAo.Controls.Add(this.panel2);
			this.tpQuanAo.Controls.Add(this.panel1);
			this.tpQuanAo.Location = new System.Drawing.Point(4, 31);
			this.tpQuanAo.Name = "tpQuanAo";
			this.tpQuanAo.Padding = new System.Windows.Forms.Padding(3);
			this.tpQuanAo.Size = new System.Drawing.Size(1056, 646);
			this.tpQuanAo.TabIndex = 2;
			this.tpQuanAo.Text = "Quần Áo";
			this.tpQuanAo.UseVisualStyleBackColor = true;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.txtGhiChuQA);
			this.panel4.Controls.Add(this.label8);
			this.panel4.Controls.Add(this.btnBrowseHinhQA);
			this.panel4.Controls.Add(this.panel25);
			this.panel4.Controls.Add(this.pbHinhQA);
			this.panel4.Controls.Add(this.panel9);
			this.panel4.Controls.Add(this.panel8);
			this.panel4.Controls.Add(this.panel7);
			this.panel4.Controls.Add(this.panel6);
			this.panel4.Controls.Add(this.panel5);
			this.panel4.Location = new System.Drawing.Point(565, 116);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(483, 527);
			this.panel4.TabIndex = 3;
			// 
			// txtGhiChuQA
			// 
			this.txtGhiChuQA.Location = new System.Drawing.Point(231, 123);
			this.txtGhiChuQA.Multiline = true;
			this.txtGhiChuQA.Name = "txtGhiChuQA";
			this.txtGhiChuQA.Size = new System.Drawing.Size(229, 138);
			this.txtGhiChuQA.TabIndex = 10;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(316, 94);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(78, 22);
			this.label8.TabIndex = 9;
			this.label8.Text = "Ghi Chú";
			// 
			// btnBrowseHinhQA
			// 
			this.btnBrowseHinhQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnBrowseHinhQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBrowseHinhQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnBrowseHinhQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnBrowseHinhQA.Location = new System.Drawing.Point(231, 28);
			this.btnBrowseHinhQA.Name = "btnBrowseHinhQA";
			this.btnBrowseHinhQA.Size = new System.Drawing.Size(229, 54);
			this.btnBrowseHinhQA.TabIndex = 8;
			this.btnBrowseHinhQA.Text = "Tải ảnh";
			this.btnBrowseHinhQA.UseVisualStyleBackColor = true;
			this.btnBrowseHinhQA.Click += new System.EventHandler(this.btnBrowseHinhQA_Click);
			// 
			// panel25
			// 
			this.panel25.Controls.Add(this.btn_QA_AddLoai);
			this.panel25.Controls.Add(this.cbLoaiQA);
			this.panel25.Controls.Add(this.label14);
			this.panel25.Location = new System.Drawing.Point(22, 356);
			this.panel25.Name = "panel25";
			this.panel25.Size = new System.Drawing.Size(438, 33);
			this.panel25.TabIndex = 6;
			// 
			// cbLoaiQA
			// 
			this.cbLoaiQA.FormattingEnabled = true;
			this.cbLoaiQA.Location = new System.Drawing.Point(147, 3);
			this.cbLoaiQA.Name = "cbLoaiQA";
			this.cbLoaiQA.Size = new System.Drawing.Size(220, 30);
			this.cbLoaiQA.TabIndex = 1;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(17, 7);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(125, 22);
			this.label14.TabIndex = 0;
			this.label14.Text = "Loại Quần Áo";
			// 
			// pbHinhQA
			// 
			this.pbHinhQA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pbHinhQA.Location = new System.Drawing.Point(22, 28);
			this.pbHinhQA.Name = "pbHinhQA";
			this.pbHinhQA.Size = new System.Drawing.Size(186, 233);
			this.pbHinhQA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbHinhQA.TabIndex = 5;
			this.pbHinhQA.TabStop = false;
			// 
			// panel9
			// 
			this.panel9.Controls.Add(this.nmGiaBanQA);
			this.panel9.Controls.Add(this.label5);
			this.panel9.Location = new System.Drawing.Point(22, 472);
			this.panel9.Name = "panel9";
			this.panel9.Size = new System.Drawing.Size(438, 33);
			this.panel9.TabIndex = 4;
			// 
			// nmGiaBanQA
			// 
			this.nmGiaBanQA.Increment = new decimal(new int[] {
            20000,
            0,
            0,
            0});
			this.nmGiaBanQA.Location = new System.Drawing.Point(147, 5);
			this.nmGiaBanQA.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
			this.nmGiaBanQA.Name = "nmGiaBanQA";
			this.nmGiaBanQA.Size = new System.Drawing.Size(278, 29);
			this.nmGiaBanQA.TabIndex = 2;
			this.nmGiaBanQA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(17, 7);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 22);
			this.label5.TabIndex = 0;
			this.label5.Text = "Giá Bán";
			// 
			// panel8
			// 
			this.panel8.Controls.Add(this.lblDaban);
			this.panel8.Controls.Add(this.lblDB);
			this.panel8.Controls.Add(this.nmSoLuongQA);
			this.panel8.Controls.Add(this.label4);
			this.panel8.Location = new System.Drawing.Point(22, 433);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(438, 33);
			this.panel8.TabIndex = 3;
			// 
			// lblDaban
			// 
			this.lblDaban.AutoSize = true;
			this.lblDaban.Location = new System.Drawing.Point(353, 6);
			this.lblDaban.Name = "lblDaban";
			this.lblDaban.Size = new System.Drawing.Size(17, 22);
			this.lblDaban.TabIndex = 3;
			this.lblDaban.Text = "*";
			// 
			// lblDB
			// 
			this.lblDB.AutoSize = true;
			this.lblDB.Location = new System.Drawing.Point(262, 6);
			this.lblDB.Name = "lblDB";
			this.lblDB.Size = new System.Drawing.Size(72, 22);
			this.lblDB.TabIndex = 2;
			this.lblDB.Text = "Đã Bán";
			// 
			// nmSoLuongQA
			// 
			this.nmSoLuongQA.Location = new System.Drawing.Point(147, 4);
			this.nmSoLuongQA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nmSoLuongQA.Name = "nmSoLuongQA";
			this.nmSoLuongQA.Size = new System.Drawing.Size(89, 29);
			this.nmSoLuongQA.TabIndex = 1;
			this.nmSoLuongQA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(17, 6);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 22);
			this.label4.TabIndex = 0;
			this.label4.Text = "Số Lượng";
			// 
			// panel7
			// 
			this.panel7.Controls.Add(this.txtSizeQA);
			this.panel7.Controls.Add(this.label3);
			this.panel7.Location = new System.Drawing.Point(22, 396);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(438, 31);
			this.panel7.TabIndex = 2;
			// 
			// txtSizeQA
			// 
			this.txtSizeQA.Location = new System.Drawing.Point(147, 3);
			this.txtSizeQA.Name = "txtSizeQA";
			this.txtSizeQA.Size = new System.Drawing.Size(278, 29);
			this.txtSizeQA.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(17, 6);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(47, 22);
			this.label3.TabIndex = 0;
			this.label3.Text = "Size";
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.txtTenQA);
			this.panel6.Controls.Add(this.label2);
			this.panel6.Location = new System.Drawing.Point(22, 318);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(438, 32);
			this.panel6.TabIndex = 1;
			// 
			// txtTenQA
			// 
			this.txtTenQA.Location = new System.Drawing.Point(147, 3);
			this.txtTenQA.Name = "txtTenQA";
			this.txtTenQA.Size = new System.Drawing.Size(278, 29);
			this.txtTenQA.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(17, 6);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(122, 22);
			this.label2.TabIndex = 0;
			this.label2.Text = "Tên Quần Áo";
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.txtIDQA);
			this.panel5.Controls.Add(this.label1);
			this.panel5.Location = new System.Drawing.Point(22, 279);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(438, 33);
			this.panel5.TabIndex = 0;
			// 
			// txtIDQA
			// 
			this.txtIDQA.Location = new System.Drawing.Point(147, 3);
			this.txtIDQA.Name = "txtIDQA";
			this.txtIDQA.ReadOnly = true;
			this.txtIDQA.Size = new System.Drawing.Size(278, 29);
			this.txtIDQA.TabIndex = 1;
			this.txtIDQA.TextChanged += new System.EventHandler(this.txtIDQA_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(17, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(30, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "ID";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.dtgvQuanAo);
			this.panel3.Location = new System.Drawing.Point(9, 114);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(550, 529);
			this.panel3.TabIndex = 2;
			// 
			// dtgvQuanAo
			// 
			this.dtgvQuanAo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dtgvQuanAo.BackgroundColor = System.Drawing.Color.White;
			this.dtgvQuanAo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dtgvQuanAo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dtgvQuanAo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dtgvQuanAo.Location = new System.Drawing.Point(0, 0);
			this.dtgvQuanAo.MultiSelect = false;
			this.dtgvQuanAo.Name = "dtgvQuanAo";
			this.dtgvQuanAo.ReadOnly = true;
			this.dtgvQuanAo.RowHeadersWidth = 51;
			this.dtgvQuanAo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dtgvQuanAo.Size = new System.Drawing.Size(550, 529);
			this.dtgvQuanAo.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.txtTimDenGiaQA);
			this.panel2.Controls.Add(this.txtTimTuGiaQA);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Controls.Add(this.label16);
			this.panel2.Controls.Add(this.txtTimQA);
			this.panel2.Controls.Add(this.btnTimQA);
			this.panel2.Location = new System.Drawing.Point(565, 7);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(483, 100);
			this.panel2.TabIndex = 1;
			// 
			// txtTimDenGiaQA
			// 
			this.txtTimDenGiaQA.Location = new System.Drawing.Point(231, 68);
			this.txtTimDenGiaQA.Name = "txtTimDenGiaQA";
			this.txtTimDenGiaQA.Size = new System.Drawing.Size(127, 29);
			this.txtTimDenGiaQA.TabIndex = 6;
			// 
			// txtTimTuGiaQA
			// 
			this.txtTimTuGiaQA.Location = new System.Drawing.Point(231, 36);
			this.txtTimTuGiaQA.Name = "txtTimTuGiaQA";
			this.txtTimTuGiaQA.Size = new System.Drawing.Size(127, 29);
			this.txtTimTuGiaQA.TabIndex = 5;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(273, 7);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(39, 22);
			this.label17.TabIndex = 4;
			this.label17.Text = "Giá";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(64, 7);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(122, 22);
			this.label16.TabIndex = 3;
			this.label16.Text = "Tên Quần Áo";
			// 
			// txtTimQA
			// 
			this.txtTimQA.Location = new System.Drawing.Point(22, 53);
			this.txtTimQA.Name = "txtTimQA";
			this.txtTimQA.Size = new System.Drawing.Size(186, 29);
			this.txtTimQA.TabIndex = 2;
			// 
			// btnTimQA
			// 
			this.btnTimQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnTimQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTimQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTimQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnTimQA.Location = new System.Drawing.Point(375, 24);
			this.btnTimQA.Name = "btnTimQA";
			this.btnTimQA.Size = new System.Drawing.Size(85, 54);
			this.btnTimQA.TabIndex = 1;
			this.btnTimQA.Text = "Tìm Kiếm";
			this.btnTimQA.UseVisualStyleBackColor = true;
			this.btnTimQA.Click += new System.EventHandler(this.btnTimQA_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnXoaQA);
			this.panel1.Controls.Add(this.btnXemQA);
			this.panel1.Controls.Add(this.btnSuaQA);
			this.panel1.Controls.Add(this.btnThemQA);
			this.panel1.Location = new System.Drawing.Point(9, 7);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(550, 100);
			this.panel1.TabIndex = 0;
			// 
			// btnXoaQA
			// 
			this.btnXoaQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnXoaQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXoaQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXoaQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnXoaQA.Location = new System.Drawing.Point(297, 24);
			this.btnXoaQA.Name = "btnXoaQA";
			this.btnXoaQA.Size = new System.Drawing.Size(85, 54);
			this.btnXoaQA.TabIndex = 4;
			this.btnXoaQA.Text = "Xóa";
			this.btnXoaQA.UseVisualStyleBackColor = true;
			this.btnXoaQA.Click += new System.EventHandler(this.btnXoaQA_Click);
			// 
			// btnXemQA
			// 
			this.btnXemQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnXemQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXemQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXemQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnXemQA.Location = new System.Drawing.Point(435, 24);
			this.btnXemQA.Name = "btnXemQA";
			this.btnXemQA.Size = new System.Drawing.Size(85, 54);
			this.btnXemQA.TabIndex = 3;
			this.btnXemQA.Text = "Xem";
			this.btnXemQA.UseVisualStyleBackColor = true;
			this.btnXemQA.Click += new System.EventHandler(this.btnXemQA_Click);
			// 
			// btnSuaQA
			// 
			this.btnSuaQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnSuaQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSuaQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSuaQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnSuaQA.Location = new System.Drawing.Point(159, 24);
			this.btnSuaQA.Name = "btnSuaQA";
			this.btnSuaQA.Size = new System.Drawing.Size(85, 54);
			this.btnSuaQA.TabIndex = 2;
			this.btnSuaQA.Text = "Sửa";
			this.btnSuaQA.UseVisualStyleBackColor = true;
			this.btnSuaQA.Click += new System.EventHandler(this.btnSuaQA_Click);
			// 
			// btnThemQA
			// 
			this.btnThemQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnThemQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThemQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThemQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnThemQA.Location = new System.Drawing.Point(26, 24);
			this.btnThemQA.Name = "btnThemQA";
			this.btnThemQA.Size = new System.Drawing.Size(85, 54);
			this.btnThemQA.TabIndex = 0;
			this.btnThemQA.Text = "Thêm";
			this.btnThemQA.UseVisualStyleBackColor = true;
			this.btnThemQA.Click += new System.EventHandler(this.btnThemQA_Click);
			// 
			// tpLoaiQuanAo
			// 
			this.tpLoaiQuanAo.Controls.Add(this.panel12);
			this.tpLoaiQuanAo.Controls.Add(this.panel10);
			this.tpLoaiQuanAo.Controls.Add(this.panel16);
			this.tpLoaiQuanAo.Controls.Add(this.panel18);
			this.tpLoaiQuanAo.Location = new System.Drawing.Point(4, 31);
			this.tpLoaiQuanAo.Name = "tpLoaiQuanAo";
			this.tpLoaiQuanAo.Padding = new System.Windows.Forms.Padding(3);
			this.tpLoaiQuanAo.Size = new System.Drawing.Size(1056, 646);
			this.tpLoaiQuanAo.TabIndex = 3;
			this.tpLoaiQuanAo.Text = "Loại Quần Áo";
			this.tpLoaiQuanAo.UseVisualStyleBackColor = true;
			// 
			// panel12
			// 
			this.panel12.Controls.Add(this.label19);
			this.panel12.Controls.Add(this.txtLoaiQA_TimKiemTen);
			this.panel12.Controls.Add(this.btnLoaiQA_TimKiem);
			this.panel12.Location = new System.Drawing.Point(565, 8);
			this.panel12.Name = "panel12";
			this.panel12.Size = new System.Drawing.Size(483, 100);
			this.panel12.TabIndex = 8;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(7, 40);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(168, 22);
			this.label19.TabIndex = 3;
			this.label19.Text = "Tên Loại Quần Áo:";
			// 
			// txtLoaiQA_TimKiemTen
			// 
			this.txtLoaiQA_TimKiemTen.Location = new System.Drawing.Point(142, 37);
			this.txtLoaiQA_TimKiemTen.Name = "txtLoaiQA_TimKiemTen";
			this.txtLoaiQA_TimKiemTen.Size = new System.Drawing.Size(227, 29);
			this.txtLoaiQA_TimKiemTen.TabIndex = 2;
			// 
			// btnLoaiQA_TimKiem
			// 
			this.btnLoaiQA_TimKiem.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnLoaiQA_TimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLoaiQA_TimKiem.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLoaiQA_TimKiem.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnLoaiQA_TimKiem.Location = new System.Drawing.Point(375, 21);
			this.btnLoaiQA_TimKiem.Name = "btnLoaiQA_TimKiem";
			this.btnLoaiQA_TimKiem.Size = new System.Drawing.Size(85, 54);
			this.btnLoaiQA_TimKiem.TabIndex = 1;
			this.btnLoaiQA_TimKiem.Text = "Tìm Kiếm";
			this.btnLoaiQA_TimKiem.UseVisualStyleBackColor = true;
			this.btnLoaiQA_TimKiem.Click += new System.EventHandler(this.btnLoaiQA_TimKiem_Click);
			// 
			// panel10
			// 
			this.panel10.Controls.Add(this.gbLoaiQA);
			this.panel10.Controls.Add(this.panel17);
			this.panel10.Controls.Add(this.panel14);
			this.panel10.Controls.Add(this.panel15);
			this.panel10.Location = new System.Drawing.Point(565, 114);
			this.panel10.Name = "panel10";
			this.panel10.Size = new System.Drawing.Size(483, 529);
			this.panel10.TabIndex = 7;
			// 
			// gbLoaiQA
			// 
			this.gbLoaiQA.Controls.Add(this.dgvSoluongLQA);
			this.gbLoaiQA.Location = new System.Drawing.Point(22, 224);
			this.gbLoaiQA.Name = "gbLoaiQA";
			this.gbLoaiQA.Size = new System.Drawing.Size(438, 295);
			this.gbLoaiQA.TabIndex = 3;
			this.gbLoaiQA.TabStop = false;
			this.gbLoaiQA.Text = "Số lượng sản phẩm theo loại";
			// 
			// dgvSoluongLQA
			// 
			this.dgvSoluongLQA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvSoluongLQA.BackgroundColor = System.Drawing.Color.White;
			this.dgvSoluongLQA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvSoluongLQA.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvSoluongLQA.Location = new System.Drawing.Point(3, 25);
			this.dgvSoluongLQA.Name = "dgvSoluongLQA";
			this.dgvSoluongLQA.ReadOnly = true;
			this.dgvSoluongLQA.RowHeadersWidth = 51;
			this.dgvSoluongLQA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvSoluongLQA.Size = new System.Drawing.Size(432, 267);
			this.dgvSoluongLQA.TabIndex = 0;
			// 
			// panel17
			// 
			this.panel17.Controls.Add(this.txtLoaiQA_SoLuongSP);
			this.panel17.Controls.Add(this.label22);
			this.panel17.Location = new System.Drawing.Point(22, 144);
			this.panel17.Name = "panel17";
			this.panel17.Size = new System.Drawing.Size(438, 52);
			this.panel17.TabIndex = 2;
			// 
			// txtLoaiQA_SoLuongSP
			// 
			this.txtLoaiQA_SoLuongSP.Location = new System.Drawing.Point(183, 14);
			this.txtLoaiQA_SoLuongSP.Name = "txtLoaiQA_SoLuongSP";
			this.txtLoaiQA_SoLuongSP.ReadOnly = true;
			this.txtLoaiQA_SoLuongSP.Size = new System.Drawing.Size(242, 29);
			this.txtLoaiQA_SoLuongSP.TabIndex = 1;
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(17, 17);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(176, 22);
			this.label22.TabIndex = 0;
			this.label22.Text = "Số lượng sản phẩm";
			// 
			// panel14
			// 
			this.panel14.Controls.Add(this.txtLoaiQA_Ten);
			this.panel14.Controls.Add(this.label9);
			this.panel14.Location = new System.Drawing.Point(22, 86);
			this.panel14.Name = "panel14";
			this.panel14.Size = new System.Drawing.Size(438, 52);
			this.panel14.TabIndex = 1;
			// 
			// txtLoaiQA_Ten
			// 
			this.txtLoaiQA_Ten.Location = new System.Drawing.Point(183, 14);
			this.txtLoaiQA_Ten.Name = "txtLoaiQA_Ten";
			this.txtLoaiQA_Ten.Size = new System.Drawing.Size(242, 29);
			this.txtLoaiQA_Ten.TabIndex = 1;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(17, 17);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(163, 22);
			this.label9.TabIndex = 0;
			this.label9.Text = "Tên Loại Quần Áo";
			// 
			// panel15
			// 
			this.panel15.Controls.Add(this.txtLoaiQA_ID);
			this.panel15.Controls.Add(this.label10);
			this.panel15.Location = new System.Drawing.Point(22, 28);
			this.panel15.Name = "panel15";
			this.panel15.Size = new System.Drawing.Size(438, 52);
			this.panel15.TabIndex = 0;
			// 
			// txtLoaiQA_ID
			// 
			this.txtLoaiQA_ID.Location = new System.Drawing.Point(183, 14);
			this.txtLoaiQA_ID.Name = "txtLoaiQA_ID";
			this.txtLoaiQA_ID.ReadOnly = true;
			this.txtLoaiQA_ID.Size = new System.Drawing.Size(242, 29);
			this.txtLoaiQA_ID.TabIndex = 1;
			this.txtLoaiQA_ID.TextChanged += new System.EventHandler(this.txtLoaiQA_ID_TextChanged);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(17, 17);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(30, 22);
			this.label10.TabIndex = 0;
			this.label10.Text = "ID";
			// 
			// panel16
			// 
			this.panel16.Controls.Add(this.dtgvLoaiQuanAo);
			this.panel16.Location = new System.Drawing.Point(9, 114);
			this.panel16.Name = "panel16";
			this.panel16.Size = new System.Drawing.Size(550, 529);
			this.panel16.TabIndex = 6;
			// 
			// dtgvLoaiQuanAo
			// 
			this.dtgvLoaiQuanAo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dtgvLoaiQuanAo.BackgroundColor = System.Drawing.Color.White;
			this.dtgvLoaiQuanAo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dtgvLoaiQuanAo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dtgvLoaiQuanAo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dtgvLoaiQuanAo.Location = new System.Drawing.Point(0, 0);
			this.dtgvLoaiQuanAo.MultiSelect = false;
			this.dtgvLoaiQuanAo.Name = "dtgvLoaiQuanAo";
			this.dtgvLoaiQuanAo.ReadOnly = true;
			this.dtgvLoaiQuanAo.RowHeadersWidth = 51;
			this.dtgvLoaiQuanAo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dtgvLoaiQuanAo.Size = new System.Drawing.Size(550, 529);
			this.dtgvLoaiQuanAo.TabIndex = 2;
			// 
			// panel18
			// 
			this.panel18.Controls.Add(this.btnXoaLoaiQA);
			this.panel18.Controls.Add(this.btnXemLoaiQA);
			this.panel18.Controls.Add(this.btnSuaLoaiQA);
			this.panel18.Controls.Add(this.btnThemLoaiQA);
			this.panel18.Location = new System.Drawing.Point(9, 7);
			this.panel18.Name = "panel18";
			this.panel18.Size = new System.Drawing.Size(550, 100);
			this.panel18.TabIndex = 4;
			// 
			// btnXoaLoaiQA
			// 
			this.btnXoaLoaiQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnXoaLoaiQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXoaLoaiQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXoaLoaiQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnXoaLoaiQA.Location = new System.Drawing.Point(297, 24);
			this.btnXoaLoaiQA.Name = "btnXoaLoaiQA";
			this.btnXoaLoaiQA.Size = new System.Drawing.Size(85, 54);
			this.btnXoaLoaiQA.TabIndex = 4;
			this.btnXoaLoaiQA.Text = "Xóa";
			this.btnXoaLoaiQA.UseVisualStyleBackColor = true;
			this.btnXoaLoaiQA.Click += new System.EventHandler(this.btnXoaLoaiQA_Click);
			// 
			// btnXemLoaiQA
			// 
			this.btnXemLoaiQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnXemLoaiQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXemLoaiQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXemLoaiQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnXemLoaiQA.Location = new System.Drawing.Point(435, 24);
			this.btnXemLoaiQA.Name = "btnXemLoaiQA";
			this.btnXemLoaiQA.Size = new System.Drawing.Size(85, 54);
			this.btnXemLoaiQA.TabIndex = 3;
			this.btnXemLoaiQA.Text = "Xem";
			this.btnXemLoaiQA.UseVisualStyleBackColor = true;
			this.btnXemLoaiQA.Click += new System.EventHandler(this.btnXemLoaiQA_Click);
			// 
			// btnSuaLoaiQA
			// 
			this.btnSuaLoaiQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnSuaLoaiQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSuaLoaiQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSuaLoaiQA.ForeColor = System.Drawing.Color.DarkBlue;
			this.btnSuaLoaiQA.Location = new System.Drawing.Point(159, 24);
			this.btnSuaLoaiQA.Name = "btnSuaLoaiQA";
			this.btnSuaLoaiQA.Size = new System.Drawing.Size(85, 54);
			this.btnSuaLoaiQA.TabIndex = 2;
			this.btnSuaLoaiQA.Text = "Sửa";
			this.btnSuaLoaiQA.UseVisualStyleBackColor = true;
			this.btnSuaLoaiQA.Click += new System.EventHandler(this.btnSuaLoaiQA_Click);
			// 
			// btnThemLoaiQA
			// 
			this.btnThemLoaiQA.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnThemLoaiQA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThemLoaiQA.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThemLoaiQA.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnThemLoaiQA.Location = new System.Drawing.Point(26, 24);
			this.btnThemLoaiQA.Name = "btnThemLoaiQA";
			this.btnThemLoaiQA.Size = new System.Drawing.Size(85, 54);
			this.btnThemLoaiQA.TabIndex = 0;
			this.btnThemLoaiQA.Text = "Thêm";
			this.btnThemLoaiQA.UseVisualStyleBackColor = true;
			this.btnThemLoaiQA.Click += new System.EventHandler(this.btnThemLoaiQA_Click);
			// 
			// tpKhachHang
			// 
			this.tpKhachHang.Controls.Add(this.groupBox1);
			this.tpKhachHang.Controls.Add(this.panel20);
			this.tpKhachHang.Controls.Add(this.panel26);
			this.tpKhachHang.Location = new System.Drawing.Point(4, 31);
			this.tpKhachHang.Name = "tpKhachHang";
			this.tpKhachHang.Padding = new System.Windows.Forms.Padding(3);
			this.tpKhachHang.Size = new System.Drawing.Size(1056, 646);
			this.tpKhachHang.TabIndex = 4;
			this.tpKhachHang.Text = "Khách Hàng";
			this.tpKhachHang.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
			this.groupBox1.Controls.Add(this.btnKhachHang_TaiLaiDS);
			this.groupBox1.Controls.Add(this.btnKhachHang_XoaBoLoc);
			this.groupBox1.Controls.Add(this.txtKhachHang_TimKiem_TenSDT);
			this.groupBox1.Location = new System.Drawing.Point(565, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(483, 87);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Tìm kiếm khách hàng theo tên hoặc số điện thoại";
			// 
			// btnKhachHang_TaiLaiDS
			// 
			this.btnKhachHang_TaiLaiDS.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnKhachHang_TaiLaiDS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnKhachHang_TaiLaiDS.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnKhachHang_TaiLaiDS.ForeColor = System.Drawing.Color.DarkBlue;
			this.btnKhachHang_TaiLaiDS.Location = new System.Drawing.Point(306, 53);
			this.btnKhachHang_TaiLaiDS.Name = "btnKhachHang_TaiLaiDS";
			this.btnKhachHang_TaiLaiDS.Size = new System.Drawing.Size(154, 25);
			this.btnKhachHang_TaiLaiDS.TabIndex = 2;
			this.btnKhachHang_TaiLaiDS.Text = "Tải lại danh sách";
			this.btnKhachHang_TaiLaiDS.UseVisualStyleBackColor = true;
			this.btnKhachHang_TaiLaiDS.Click += new System.EventHandler(this.btnKhachHang_TaiLaiDS_Click);
			// 
			// btnKhachHang_XoaBoLoc
			// 
			this.btnKhachHang_XoaBoLoc.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnKhachHang_XoaBoLoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnKhachHang_XoaBoLoc.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnKhachHang_XoaBoLoc.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnKhachHang_XoaBoLoc.Location = new System.Drawing.Point(42, 53);
			this.btnKhachHang_XoaBoLoc.Name = "btnKhachHang_XoaBoLoc";
			this.btnKhachHang_XoaBoLoc.Size = new System.Drawing.Size(111, 25);
			this.btnKhachHang_XoaBoLoc.TabIndex = 1;
			this.btnKhachHang_XoaBoLoc.Text = "Xóa bộ lọc";
			this.btnKhachHang_XoaBoLoc.UseVisualStyleBackColor = true;
			this.btnKhachHang_XoaBoLoc.Click += new System.EventHandler(this.btnKhachHang_XoaBoLoc_Click);
			// 
			// txtKhachHang_TimKiem_TenSDT
			// 
			this.txtKhachHang_TimKiem_TenSDT.Location = new System.Drawing.Point(42, 24);
			this.txtKhachHang_TimKiem_TenSDT.Name = "txtKhachHang_TimKiem_TenSDT";
			this.txtKhachHang_TimKiem_TenSDT.Size = new System.Drawing.Size(418, 29);
			this.txtKhachHang_TimKiem_TenSDT.TabIndex = 0;
			this.txtKhachHang_TimKiem_TenSDT.TextChanged += new System.EventHandler(this.txtKhachHang_TimKiem_TenSDT_TextChanged);
			// 
			// panel20
			// 
			this.panel20.Controls.Add(this.panel28);
			this.panel20.Controls.Add(this.panel22);
			this.panel20.Controls.Add(this.panel23);
			this.panel20.Controls.Add(this.panel24);
			this.panel20.Location = new System.Drawing.Point(565, 101);
			this.panel20.Name = "panel20";
			this.panel20.Size = new System.Drawing.Size(483, 542);
			this.panel20.TabIndex = 7;
			// 
			// panel28
			// 
			this.panel28.Controls.Add(this.txtKhachHang_DiaChi);
			this.panel28.Controls.Add(this.label15);
			this.panel28.Location = new System.Drawing.Point(22, 197);
			this.panel28.Name = "panel28";
			this.panel28.Size = new System.Drawing.Size(438, 106);
			this.panel28.TabIndex = 4;
			// 
			// txtKhachHang_DiaChi
			// 
			this.txtKhachHang_DiaChi.Location = new System.Drawing.Point(180, 17);
			this.txtKhachHang_DiaChi.Multiline = true;
			this.txtKhachHang_DiaChi.Name = "txtKhachHang_DiaChi";
			this.txtKhachHang_DiaChi.Size = new System.Drawing.Size(245, 86);
			this.txtKhachHang_DiaChi.TabIndex = 1;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(17, 17);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(71, 22);
			this.label15.TabIndex = 0;
			this.label15.Text = "Địa Chỉ";
			// 
			// panel22
			// 
			this.panel22.Controls.Add(this.txtKhachHang_SoLuongHoaDon);
			this.panel22.Controls.Add(this.label11);
			this.panel22.Location = new System.Drawing.Point(22, 309);
			this.panel22.Name = "panel22";
			this.panel22.Size = new System.Drawing.Size(438, 52);
			this.panel22.TabIndex = 3;
			// 
			// txtKhachHang_SoLuongHoaDon
			// 
			this.txtKhachHang_SoLuongHoaDon.Location = new System.Drawing.Point(180, 14);
			this.txtKhachHang_SoLuongHoaDon.Name = "txtKhachHang_SoLuongHoaDon";
			this.txtKhachHang_SoLuongHoaDon.ReadOnly = true;
			this.txtKhachHang_SoLuongHoaDon.Size = new System.Drawing.Size(245, 29);
			this.txtKhachHang_SoLuongHoaDon.TabIndex = 5;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(17, 17);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(181, 22);
			this.label11.TabIndex = 0;
			this.label11.Text = "Số Lượng  Hóa Đơn";
			// 
			// panel23
			// 
			this.panel23.Controls.Add(this.txtKhachHang_SDT);
			this.panel23.Controls.Add(this.label12);
			this.panel23.Location = new System.Drawing.Point(22, 139);
			this.panel23.Name = "panel23";
			this.panel23.Size = new System.Drawing.Size(438, 52);
			this.panel23.TabIndex = 2;
			// 
			// txtKhachHang_SDT
			// 
			this.txtKhachHang_SDT.Location = new System.Drawing.Point(180, 17);
			this.txtKhachHang_SDT.Name = "txtKhachHang_SDT";
			this.txtKhachHang_SDT.Size = new System.Drawing.Size(245, 29);
			this.txtKhachHang_SDT.TabIndex = 1;
			this.txtKhachHang_SDT.TextChanged += new System.EventHandler(this.txtSoDTKH_TextChanged);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(17, 17);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(130, 22);
			this.label12.TabIndex = 0;
			this.label12.Text = "Số Điện Thoại";
			// 
			// panel24
			// 
			this.panel24.Controls.Add(this.txtKhachHang_Ten);
			this.panel24.Controls.Add(this.label13);
			this.panel24.Location = new System.Drawing.Point(22, 81);
			this.panel24.Name = "panel24";
			this.panel24.Size = new System.Drawing.Size(438, 52);
			this.panel24.TabIndex = 1;
			// 
			// txtKhachHang_Ten
			// 
			this.txtKhachHang_Ten.Location = new System.Drawing.Point(180, 14);
			this.txtKhachHang_Ten.Name = "txtKhachHang_Ten";
			this.txtKhachHang_Ten.Size = new System.Drawing.Size(245, 29);
			this.txtKhachHang_Ten.TabIndex = 1;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(17, 17);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(150, 22);
			this.label13.TabIndex = 0;
			this.label13.Text = "Tên Khách Hàng";
			// 
			// panel26
			// 
			this.panel26.Controls.Add(this.dtgvKH);
			this.panel26.Location = new System.Drawing.Point(9, 8);
			this.panel26.Name = "panel26";
			this.panel26.Size = new System.Drawing.Size(550, 635);
			this.panel26.TabIndex = 6;
			// 
			// dtgvKH
			// 
			this.dtgvKH.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dtgvKH.BackgroundColor = System.Drawing.Color.White;
			this.dtgvKH.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dtgvKH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dtgvKH.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dtgvKH.Location = new System.Drawing.Point(0, 0);
			this.dtgvKH.MultiSelect = false;
			this.dtgvKH.Name = "dtgvKH";
			this.dtgvKH.ReadOnly = true;
			this.dtgvKH.RowHeadersWidth = 51;
			this.dtgvKH.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dtgvKH.Size = new System.Drawing.Size(550, 635);
			this.dtgvKH.TabIndex = 2;
			// 
			// tpTaiKhoan
			// 
			this.tpTaiKhoan.Controls.Add(this.panel21);
			this.tpTaiKhoan.Controls.Add(this.panel32);
			this.tpTaiKhoan.Location = new System.Drawing.Point(4, 31);
			this.tpTaiKhoan.Name = "tpTaiKhoan";
			this.tpTaiKhoan.Padding = new System.Windows.Forms.Padding(3);
			this.tpTaiKhoan.Size = new System.Drawing.Size(1056, 646);
			this.tpTaiKhoan.TabIndex = 5;
			this.tpTaiKhoan.Text = "Tài Khoản";
			this.tpTaiKhoan.UseVisualStyleBackColor = true;
			// 
			// panel21
			// 
			this.panel21.Controls.Add(this.panel27);
			this.panel21.Controls.Add(this.panel33);
			this.panel21.Controls.Add(this.panel30);
			this.panel21.Controls.Add(this.panel31);
			this.panel21.Location = new System.Drawing.Point(565, 8);
			this.panel21.Name = "panel21";
			this.panel21.Size = new System.Drawing.Size(483, 635);
			this.panel21.TabIndex = 9;
			// 
			// panel27
			// 
			this.panel27.Controls.Add(this.cbTK_Loai);
			this.panel27.Controls.Add(this.label23);
			this.panel27.Location = new System.Drawing.Point(22, 222);
			this.panel27.Name = "panel27";
			this.panel27.Size = new System.Drawing.Size(438, 52);
			this.panel27.TabIndex = 6;
			// 
			// cbTK_Loai
			// 
			this.cbTK_Loai.FormattingEnabled = true;
			this.cbTK_Loai.Location = new System.Drawing.Point(180, 14);
			this.cbTK_Loai.Name = "cbTK_Loai";
			this.cbTK_Loai.Size = new System.Drawing.Size(245, 30);
			this.cbTK_Loai.TabIndex = 1;
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(17, 17);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(136, 22);
			this.label23.TabIndex = 0;
			this.label23.Text = "Loại Tài Khoản";
			// 
			// panel33
			// 
			this.panel33.Controls.Add(this.btnTK_Xoa);
			this.panel33.Controls.Add(this.btnTK_Them);
			this.panel33.Location = new System.Drawing.Point(22, 423);
			this.panel33.Name = "panel33";
			this.panel33.Size = new System.Drawing.Size(438, 81);
			this.panel33.TabIndex = 5;
			// 
			// btnTK_Xoa
			// 
			this.btnTK_Xoa.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnTK_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTK_Xoa.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTK_Xoa.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnTK_Xoa.Location = new System.Drawing.Point(340, 13);
			this.btnTK_Xoa.Name = "btnTK_Xoa";
			this.btnTK_Xoa.Size = new System.Drawing.Size(85, 54);
			this.btnTK_Xoa.TabIndex = 2;
			this.btnTK_Xoa.Text = "Xóa";
			this.btnTK_Xoa.UseVisualStyleBackColor = true;
			this.btnTK_Xoa.Click += new System.EventHandler(this.btnTK_Xoa_Click);
			// 
			// btnTK_Them
			// 
			this.btnTK_Them.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btnTK_Them.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTK_Them.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTK_Them.ForeColor = System.Drawing.Color.MediumBlue;
			this.btnTK_Them.Location = new System.Drawing.Point(20, 13);
			this.btnTK_Them.Name = "btnTK_Them";
			this.btnTK_Them.Size = new System.Drawing.Size(85, 54);
			this.btnTK_Them.TabIndex = 1;
			this.btnTK_Them.Text = "Thêm";
			this.btnTK_Them.UseVisualStyleBackColor = true;
			this.btnTK_Them.Click += new System.EventHandler(this.btnTK_Them_Click);
			// 
			// panel30
			// 
			this.panel30.Controls.Add(this.txtTK_MatKhau);
			this.panel30.Controls.Add(this.label25);
			this.panel30.Location = new System.Drawing.Point(22, 289);
			this.panel30.Name = "panel30";
			this.panel30.Size = new System.Drawing.Size(438, 52);
			this.panel30.TabIndex = 2;
			// 
			// txtTK_MatKhau
			// 
			this.txtTK_MatKhau.Location = new System.Drawing.Point(180, 17);
			this.txtTK_MatKhau.Name = "txtTK_MatKhau";
			this.txtTK_MatKhau.Size = new System.Drawing.Size(245, 29);
			this.txtTK_MatKhau.TabIndex = 1;
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(17, 17);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(88, 22);
			this.label25.TabIndex = 0;
			this.label25.Text = "Mật Khẩu";
			// 
			// panel31
			// 
			this.panel31.Controls.Add(this.txtTK_TenDangNhap);
			this.panel31.Controls.Add(this.label26);
			this.panel31.Location = new System.Drawing.Point(22, 154);
			this.panel31.Name = "panel31";
			this.panel31.Size = new System.Drawing.Size(438, 52);
			this.panel31.TabIndex = 1;
			// 
			// txtTK_TenDangNhap
			// 
			this.txtTK_TenDangNhap.Location = new System.Drawing.Point(180, 14);
			this.txtTK_TenDangNhap.Name = "txtTK_TenDangNhap";
			this.txtTK_TenDangNhap.Size = new System.Drawing.Size(245, 29);
			this.txtTK_TenDangNhap.TabIndex = 1;
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(17, 17);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(133, 22);
			this.label26.TabIndex = 0;
			this.label26.Text = "Tên Tài Khoản";
			// 
			// panel32
			// 
			this.panel32.Controls.Add(this.dtgvTaiKhoan);
			this.panel32.Location = new System.Drawing.Point(9, 8);
			this.panel32.Name = "panel32";
			this.panel32.Size = new System.Drawing.Size(550, 635);
			this.panel32.TabIndex = 8;
			// 
			// dtgvTaiKhoan
			// 
			this.dtgvTaiKhoan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dtgvTaiKhoan.BackgroundColor = System.Drawing.Color.White;
			this.dtgvTaiKhoan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dtgvTaiKhoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dtgvTaiKhoan.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dtgvTaiKhoan.Location = new System.Drawing.Point(0, 0);
			this.dtgvTaiKhoan.MultiSelect = false;
			this.dtgvTaiKhoan.Name = "dtgvTaiKhoan";
			this.dtgvTaiKhoan.ReadOnly = true;
			this.dtgvTaiKhoan.RowHeadersWidth = 51;
			this.dtgvTaiKhoan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dtgvTaiKhoan.Size = new System.Drawing.Size(550, 635);
			this.dtgvTaiKhoan.TabIndex = 2;
			// 
			// btn_QA_AddLoai
			// 
			this.btn_QA_AddLoai.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
			this.btn_QA_AddLoai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_QA_AddLoai.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_QA_AddLoai.ForeColor = System.Drawing.Color.Crimson;
			this.btn_QA_AddLoai.Location = new System.Drawing.Point(374, 3);
			this.btn_QA_AddLoai.Name = "btn_QA_AddLoai";
			this.btn_QA_AddLoai.Size = new System.Drawing.Size(51, 25);
			this.btn_QA_AddLoai.TabIndex = 2;
			this.btn_QA_AddLoai.Text = "+";
			this.btn_QA_AddLoai.UseVisualStyleBackColor = true;
			this.btn_QA_AddLoai.Click += new System.EventHandler(this.btn_QA_AddLoai_Click);
			// 
			// frmAdmin
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1064, 681);
			this.Controls.Add(this.tcAdmin);
			this.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "frmAdmin";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Admin";
			this.tcAdmin.ResumeLayout(false);
			this.tpQuanAo.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.panel25.ResumeLayout(false);
			this.panel25.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pbHinhQA)).EndInit();
			this.panel9.ResumeLayout(false);
			this.panel9.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nmGiaBanQA)).EndInit();
			this.panel8.ResumeLayout(false);
			this.panel8.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nmSoLuongQA)).EndInit();
			this.panel7.ResumeLayout(false);
			this.panel7.PerformLayout();
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dtgvQuanAo)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.tpLoaiQuanAo.ResumeLayout(false);
			this.panel12.ResumeLayout(false);
			this.panel12.PerformLayout();
			this.panel10.ResumeLayout(false);
			this.gbLoaiQA.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvSoluongLQA)).EndInit();
			this.panel17.ResumeLayout(false);
			this.panel17.PerformLayout();
			this.panel14.ResumeLayout(false);
			this.panel14.PerformLayout();
			this.panel15.ResumeLayout(false);
			this.panel15.PerformLayout();
			this.panel16.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dtgvLoaiQuanAo)).EndInit();
			this.panel18.ResumeLayout(false);
			this.tpKhachHang.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel20.ResumeLayout(false);
			this.panel28.ResumeLayout(false);
			this.panel28.PerformLayout();
			this.panel22.ResumeLayout(false);
			this.panel22.PerformLayout();
			this.panel23.ResumeLayout(false);
			this.panel23.PerformLayout();
			this.panel24.ResumeLayout(false);
			this.panel24.PerformLayout();
			this.panel26.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dtgvKH)).EndInit();
			this.tpTaiKhoan.ResumeLayout(false);
			this.panel21.ResumeLayout(false);
			this.panel27.ResumeLayout(false);
			this.panel27.PerformLayout();
			this.panel33.ResumeLayout(false);
			this.panel30.ResumeLayout(false);
			this.panel30.PerformLayout();
			this.panel31.ResumeLayout(false);
			this.panel31.PerformLayout();
			this.panel32.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dtgvTaiKhoan)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tcAdmin;
		private System.Windows.Forms.TabPage tpQuanAo;
		private System.Windows.Forms.TabPage tpLoaiQuanAo;
		private System.Windows.Forms.TabPage tpKhachHang;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel9;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.NumericUpDown nmSoLuongQA;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.TextBox txtTenQA;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.TextBox txtIDQA;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox txtTimQA;
		private System.Windows.Forms.Button btnTimQA;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnSuaQA;
		private System.Windows.Forms.Button btnThemQA;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.TextBox txtLoaiQA_Ten;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Panel panel15;
		private System.Windows.Forms.TextBox txtLoaiQA_ID;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Panel panel16;
		private System.Windows.Forms.Panel panel18;
		private System.Windows.Forms.Button btnXemLoaiQA;
		private System.Windows.Forms.Button btnSuaLoaiQA;
		private System.Windows.Forms.Button btnThemLoaiQA;
		private System.Windows.Forms.PictureBox pbHinhQA;
		private System.Windows.Forms.Panel panel20;
		private System.Windows.Forms.Panel panel22;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Panel panel23;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Panel panel24;
		private System.Windows.Forms.TextBox txtKhachHang_Ten;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Panel panel26;
		private System.Windows.Forms.Button btnBrowseHinhQA;
		private System.Windows.Forms.Panel panel25;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox txtKhachHang_SDT;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtGhiChuQA;
		private System.Windows.Forms.NumericUpDown nmGiaBanQA;
		private System.Windows.Forms.TextBox txtSizeQA;
		private System.Windows.Forms.Panel panel28;
		private System.Windows.Forms.TextBox txtKhachHang_DiaChi;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox txtTimTuGiaQA;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtTimDenGiaQA;
		private System.Windows.Forms.ComboBox cbLoaiQA;
		private System.Windows.Forms.Button btnXemQA;
		private System.Windows.Forms.DataGridView dtgvKH;
		private System.Windows.Forms.TextBox txtKhachHang_SoLuongHoaDon;
		private System.Windows.Forms.DataGridView dtgvLoaiQuanAo;
		private System.Windows.Forms.DataGridView dtgvQuanAo;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox txtLoaiQA_TimKiemTen;
		private System.Windows.Forms.Button btnLoaiQA_TimKiem;
		private System.Windows.Forms.Panel panel17;
		private System.Windows.Forms.TextBox txtLoaiQA_SoLuongSP;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnKhachHang_TaiLaiDS;
		private System.Windows.Forms.Button btnKhachHang_XoaBoLoc;
		private System.Windows.Forms.TextBox txtKhachHang_TimKiem_TenSDT;
		private System.Windows.Forms.Button btnXoaQA;
		private System.Windows.Forms.Button btnXoaLoaiQA;
		private System.Windows.Forms.Label lblDB;
		private System.Windows.Forms.Label lblDaban;
		private System.Windows.Forms.TabPage tpTaiKhoan;
		private System.Windows.Forms.Panel panel21;
		private System.Windows.Forms.Panel panel33;
		private System.Windows.Forms.Button btnTK_Xoa;
		private System.Windows.Forms.Button btnTK_Them;
		private System.Windows.Forms.Panel panel30;
		private System.Windows.Forms.TextBox txtTK_MatKhau;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Panel panel31;
		private System.Windows.Forms.TextBox txtTK_TenDangNhap;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Panel panel32;
		private System.Windows.Forms.DataGridView dtgvTaiKhoan;
		private System.Windows.Forms.Panel panel27;
		private System.Windows.Forms.ComboBox cbTK_Loai;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.GroupBox gbLoaiQA;
		private System.Windows.Forms.DataGridView dgvSoluongLQA;
		private System.Windows.Forms.Button btn_QA_AddLoai;
	}
}